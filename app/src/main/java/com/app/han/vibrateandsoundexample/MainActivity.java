package com.app.han.vibrateandsoundexample;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button BVibrate = (Button)findViewById(R.id.btnVibrate);
        Button BSound = (Button)findViewById(R.id.btnSound);
        final Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        final MediaPlayer mp =MediaPlayer.create(this,R.raw.alarm);

        BVibrate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    v.vibrate(800);
                }
                return false;
            }
        });

        BSound.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    mp.start();
                }
                return false;
            }
        });

    }
}
